<?php
// Database connection definitions
// Please change the passwords to an other random string
define('gisdb_user','sablon_admin');
define('gisdb_pass','12345');
define('gisdb_name','gisdata');
define('gisdb_host','gisdata'); //instead of localhost using docker's gisdata host

// Project's sql table name 
#define('PROJECTTABLE','your_database_table_name');
define('PROJECTTABLE',basename(__DIR__));

// Default project data restriction level
// 0 data read/modify for everybody
// 1 data read/modify only for logined users
// 2 data read/modify only for group members
// 3 data read/modify only own data
define('ACC_LEVEL',2);
define('MOD_LEVEL',2);

// Default language
define('LANG','hu');

// Path settings
#define('OBMS','openbiomaps.org');
#define('OBMS',sprintf("%s",'localhost:9880/biomaps'));
define('PATH','/projects');
define('URL',sprintf("%s:9880%s%s",$_SERVER['SERVER_NAME'],PATH,'/'.PROJECTTABLE));
define('OAUTHURL',sprintf("http://%s%s%s",'localhost',PATH,'/'.PROJECTTABLE));

// Mapserver's variables
define('PRIVATE_MAPSERV',sprintf("%s/private/proxy.php",URL));
define('PUBLIC_MAPSERV',sprintf("%s/public/proxy.php",URL));
define('PRIVATE_MAPCACHE',sprintf("%s/private/cache.php",URL));
define('PUBLIC_MAPCACHE',sprintf("%s/public/cache.php",URL));
define('MAPSERVER','http://mapserver/cgi-bin/mapserv');
define('MAPCACHE','http://mapserver/mapcache');
define('MAP','PMAP');
define('PRIVATE_MAPFILE','private.map');

// Invitations
// 0 Only admins can invite new members, any grater value the maximum active invitations per member
define('INVITATIONS',10);

// Mail settings
define('SMTP_AUTH',false); # true
define('SMTP_HOST','...');
define('SMTP_USERNAME','...');
define('SMTP_PASSWORD','...');
define('SMTP_SECURE','tls'); # ssl
define('SMTP_PORT','587'); # 465
define('SMTP_SENDER','openbiomaps@...');

// UI Settings
define('MAP_OVER_MAINPAGE',0);
define('LOAD_INTROPAGE',0);
define('SHINYURL',false);
define('RSERVER',false);
define('LOGINPAGE','map');
define('TRAINING',false);

date_default_timezone_set('Europe/Budapest');

define('OB_PROJECT_DOMAIN',OB_DOMAIN);
?>
